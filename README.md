# Insight Data Engineering Coding Challenge


## Author
Submitted on 11/10/2016 by Ingolf Warnke, ingolf.warnke@gmail.com


## Language and Dependencies
I am using **python3**. The imported modules should be available with a standard python3 installation.


### Modules Used
* timeit
* sys
* os
* logging


## List of Important Files
* **run.sh** is the script to run the program, contains nothing but the command to run the program. Line used for manual testing is commented out. 
* **src/antifraud.py** is the main program. Input and output files are given as command line arguments
* **src/transactionsearch/market.py** is the class creating the initial state of the network and determining the degree of connection between network nodes
* A few additional files exist in several directories. These are not central to the program and/or are just being used for manual testing.

## Additional Tests
**Unit testing** was done by hand, i.e. input/output formatting and initial classifier tests, etc.
I have added a small number of critical **functional** tests to the provided testing framework "insight_testsuite".


## Notes on Algorithm and Data Structures
* I am using a BFS algorithm to efficiently determine the degree of connection between two nodes
* The primary network is held in memory by an instance of the Market class which contains traders, i.e. instances of the Trader class. Each trader holds its direct connections as well as itself in a set
* Subsequent set operations in the central instance method **market.is_trusted()** are used to determine the degree of connection between two traders. The initial set of traders and the set operations implicitly create a search tree
* The connection search stops once the lowest degree connection is found, or the maximum degree for the search is reached
* The maximum degree is set to 4, but the algorithm will do any degree until hardware memory limits are reached 


## Note on Preliminary Timings and Provided Data Set
* I have used the provided input files stream_payment.txt and batch_payment.txt for initial timings to help understand bottlenecks and limits of this BFS implementation given a typical real-world network
* However, all unique IDs (all traders) are already contained in less than the first 25% of the batch_payment.txt file. This makes the dataset less useful when trying to examine the scaling of the time required for a single connection-degree classification with network size
* How many transactions do we classify per minute? The file stream_payment.txt has about 3 Million transactions in a period of 144 minutes. The file batch_payment.txt has about 4 million transactions in a bit over 11 minutes. Assuming this to be representative, this puts the number of transactions per second somewhere between 300 and 4000.
* The parsing of the batch_payment.txt file scales more or less linearly with the size
of the file
* The majority of computation time is spent in the classification. With the data provided, the classification times per transaction range between 50 and 60 Micro seconds (on my 2.1GHz/8GB laptop). That means that, given the network provided as sample input, the algorithm can classify the degree of about 16000 transactions per second. Therefore, the algorithm should be useable to do real-life connection-monitoring
* Furthermore, it could be interesting to look at some statistics of the network: mean, median number of direct connections, and the distribution may provide the basis for a better understanding of user behavior. Including the direction of the payments could be of interest for studying user behavior as well.

## Further Considerations
* In my opinion, inclusion of 4th degree connections in the trusted network might not be strict enough to be efficient. It depends on the degree of connectedness in the particular social network. 
Using the example data provided, 48.6% of the transactions are 1st degree trusted, 78.2% are at least 2nd degree trusted (feature 2), and 92.5% of the transactions are at least 4th degree trusted (feature 3).
* One real world problem for an application like this is the existence of hubs, i.e. highly connected traders. An admittedly extreme example would be a company like Amazon.com to become a trader in our network. Almost everybody has done commerce with Amazon, and therefore almost everybody is connected to 2nd degree with everybody else. An exclusion of hubs from this algorithm would make Feature 2 and 3 more meaningful and lead to an overall speed-up as well. Hubs could be excluded by introducing a cut off threshold for the number of direct connections in the proposed screening algorithm
* What type of notification is proposed in paymo? What types of fraud could occur?
  * I accidentally sent money to the wrong person out of my network. A notification on my phone will be very useful in that case.
  * Someone stole my phone, managed to log in and sending money to people or himself. Displaying a note on that phone within the app would only be seen by the thief.


## List of proposed additional features / Efficient Fraud Protection Measures:
  * Warn user if more than one session active at the same time
  * Using the device ID, tie user's hardware to the account
  * Use MFA: trigger security code sent by SMS in case of out of network interaction
  * Make sure that a payment method (card info) can only be tied to a single account.
    When a new user (e.g. a thief) tries to register a card that is already registered with a different user account, this user should be added to a black list and most importantly, their devices should be added to a black list (via device ID).
    Technicality: A mobile payment system that stores credit card information needs to use host-proof encrypted storage of sensitive data. This makes an API side lookup of already registered cards impossible. One possible strategy would be the use of hash-tables.
  * Login from a new phone (hardware): alert SMS to phone number of account holder
  * When new payment method is being registered, run a third-party verification
    service 
  * When a new payment method is being registered, use ebay's strategy to withdraw and
    refund a small amount of money from the account and ask user for the exact
    amount that was withdrawn and refunded
  * Machine learning could be used to help classify transactions and customer behavior into trusted or suspicious. Parameters such as
    amount, number of out of network trading partners, local time, geoposition, number of connections, frequency of payments, etc.
    could be useful for the classification.
