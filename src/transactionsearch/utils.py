
def isdate(string):
    #TODO: use regex instead
    if len(string) < 19:
        return 0

    result =  string[4]  == '-'
    result *= string[7]  == '-'
    result *= string[10] == ' '
    result *= string[13] == ':'
    result *= string[16] == ':'
    return result
