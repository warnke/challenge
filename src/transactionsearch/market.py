#!/usr/local/bin/python3

import logging
from .utils import isdate

logging.basicConfig(level=logging.DEBUG)



class Trader:
    """This is a Trader (node) class in the market (network)"""

    def __init__(self, connected_traders=()):
        self.connected_traders = set(*connected_traders)
        self.connected_traders.add(self)

    def add(self, new_trader):
        self.connected_traders.add(new_trader)

    def connect(self, other_trader):
        self.add(other_trader)
        other_trader.add(self)

class Market:
    """This is the market class containing and managing the whole network of traders."""

    search_depth = 4
    logger = logging.getLogger(name="Market")

    def __init__(self, traders=dict()):
        self.traders = traders 


    def new_trader(self, trader_id):
        trader = Trader()
        self.traders[trader_id] = trader
        return trader

    def from_id(self, trader_id):
        if trader_id in self.traders:
            return self.traders[trader_id]
        else:
            return self.new_trader(trader_id)

    def connect(self, id1, id2):
        trader1 = self.from_id( id1 )
        trader2 = self.from_id( id2 )
        trader1.connect(trader2)

    def is_trusted(self, id1, id2):
        """Finding the degree of connectivity."""
        trader_1 = self.from_id( id1 )
        trader_2 = self.from_id( id2 )

        sets = [{trader_1},
                {trader_2}]
   
        #TODO: check if always updating the smaller of the two sets is faster
        for d in range(self.search_depth):
            which_to_update = d%2
            sets[which_to_update] = set.union(*(s.connected_traders for s in sets[which_to_update]))
            if not sets[0].isdisjoint(sets[1]):
              return d+1
        return -1 # no connections up to depth specified
        

    @staticmethod
    def from_file(filename):

        f = open(filename, 'r')
        col_names = f.readline().split(",")

        # Not needed for now:
        #num_cols = len(col_names)
        #values = dict()
        #for name in col_names:
        #    values[name] = []
    
        market = Market()
    
        for line in f:
            li = line.split(",")
            #li = li[:num_cols-1]
    
            if not isdate(li[0]):
              # Failsafe, should not be necessary
              # Market.logger.debug("Found broken line: {}".format(line))
                continue
            
            id1 = int(li[1])
            id2 = int(li[2])
            
            market.connect(id1, id2)
        
        f.close()
        return market
