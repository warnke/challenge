#!/usr/local/bin/python3

from transactionsearch import market

if __name__ == "__main__":

    m = market.Market()
    m.connect(1, 2)
    m.connect(2, 3)
    m.connect(3, 4)
    m.connect(4, 5)
    m.connect(5, 6)

    num_traders = len(m.traders)

    print("Found {} distinct traders.".format(num_traders))


    print("1<->2 : {}".format(m.is_trusted(1, 2)))
    print("1<->3 : {}".format(m.is_trusted(1, 3)))
    print("1<->4 : {}".format(m.is_trusted(1, 4)))
    print("1<->5 : {}".format(m.is_trusted(1, 5)))
    print("1<->6 : {}".format(m.is_trusted(1, 6)))
    print("2<->6 : {}".format(m.is_trusted(2, 6)))
