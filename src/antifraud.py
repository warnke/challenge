import os
import sys
from transactionsearch import market
from transactionsearch.utils import isdate
from timeit import default_timer as timer

if __name__ == "__main__":

    # Prelim timing to get an idea ...
    #time_start = timer()

    # I/O business
    batch_file = sys.argv[1]
    stream_file = sys.argv[2]
    out1 = open(sys.argv[3], 'w') 
    out2 = open(sys.argv[4], 'w')
    out3 = open(sys.argv[5], 'w')
    
    # Initiate network
    m = market.Market.from_file(batch_file)
    
    # Get some additional info
    #num_traders = len(m.traders)
    #print("Found {} distinct traders in batch_payment file".format(num_traders))

    # Open streaming file 
    stream = open(stream_file, 'r')
    col_names = stream.readline().split(",")
    num_cols = len(col_names)

    # classification_start_time = timer()
    for line in stream:
        li = line.split(",")
        li = li[:num_cols-1]

        if not isdate(li[0]):
            # skip lines that do not start with a date
            continue
    
        id1 = int(li[1])
        id2 = int(li[2])
    
        deg = m.is_trusted(id1, id2)

        if deg ==-1: # trader couple outside their 4th degree network
            out1.write("unverified\n")
            out2.write("unverified\n")
            out3.write("unverified\n")
        elif deg == 1: # trader couple inside 1st, 2nd, and 4th degree network
            out1.write("trusted\n")
            out2.write("trusted\n")
            out3.write("trusted\n")
        elif deg == 2: # trader couple inside 2nd, and 4th degree network
            out1.write("unverified\n")
            out2.write("trusted\n")
            out3.write("trusted\n")
        elif deg <= 4: # trader inside 3rd and 4th degree network
            out1.write("unverified\n")
            out2.write("unverified\n")
            out3.write("trusted\n")

    #classification_time = timer() - classification_start_time

    # I/O stuff, close files
    stream.close()
    out1.close()
    out2.close()
    out3.close()

    #time_end = timer()
    #print("Overall time:", time_end-time_start, "seconds")
    #print("Classification time:", classification_time, "seconds")
