#!/usr/bin/env bash

python3 ./src/antifraud.py ./paymo_input/batch_payment.txt ./paymo_input/stream_payment.txt ./paymo_output/output1.txt ./paymo_output/output2.txt ./paymo_output/output3.txt 

## M A N U A L   T E S T I N G   O N L Y ##########################
# python3 ./src/antifraud.py ./test_data/batch_test_short_line.csv ./test_data/stream_test.csv ./paymo_output/output1.txt ./paymo_output/output2.txt ./paymo_output/output3.txt 
###################################################################
